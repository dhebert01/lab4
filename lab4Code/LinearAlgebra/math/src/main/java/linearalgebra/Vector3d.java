//David Hebert 2141781
package linearalgebra;
public class Vector3d {
    final double x;
    final double y;
    final double z;
    public  Vector3d(double x, double y, double z){
        this.x = x;
        this.y = y;
        this.z = z;
    }

    public double getX(){
        return this.x;
    }

    public double getY(){
        return this.y;
    }

    public double getZ(){
        return this.z;
    }

    public double magnitude(){
        double xSqrd = Math.pow(this.x, 2);
        double ySqrd = Math.pow(this.y, 2);
        double zSqrd = Math.pow(this.z, 2);
        return Math.sqrt(xSqrd +  ySqrd + zSqrd);
    }

    public double dotProduct(Vector3d vector){
        double x = vector.getX();
        double y = vector.getY();
        double z = vector.getZ();

        return (this.x * x) + (this.y * y) + (this.z * z);
    }
    
    public Vector3d add(Vector3d vector){
         double x = this.x + vector.getX();
        double y = this.y + vector.getY();
        double z = this.z + vector.getZ();

        Vector3d temp = new Vector3d(x, y , z);
        return temp;
    }
}
